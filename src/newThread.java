/**
 *Класс {@code newThread}, инициализирующий новый поток.
 *
 * @author Петрушов Роман.
 * @since 14.11.2016
 */

public class newThread implements Runnable {
    Thread thread;
    private int count;
    private String Message;


    newThread(int priority, int count, String Message){
        thread = new Thread(this);
        thread.setPriority(priority);
        this.count = count;
        this.Message = Message;
        thread.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < count;i++) {
            System.out.println(Message);

            try {
                thread.sleep(500);
            } catch (InterruptedException e) {
            }

        }
    }
}

