/**
 * * Класс {@code ThreadDemo}, демонстрирующий cпор между яйцом и курицей.
 *
 * @author Петрушов Роман.
 * @since 14.11.2016
 */
public class ThreadDemo {
    public static void main(String[] args) {
        newThread chicken = new newThread(10, 20, "Курица");
        newThread egg = new newThread(1, 20, "Яйцо");

        try {
            chicken.thread.join();
        } catch (InterruptedException e) {
        }
        if ((!chicken.thread.isAlive()) && egg.thread.isAlive()) {
            System.out.println("Последнее слово осталось за Яйцом!");
        } else {
            System.out.println("Последнее слово осталось за Курицей!");
        }
    }
}

